### Общие переменные
ARG WHEEL_DIST="/tmp/wheels"
ARG WORK_DIR="/routes-to-vpn"

### Сборка образа с зависимостями
FROM python:3.11.2-slim as build
ARG WHEEL_DIST
COPY requirements.txt /tmp/requirements.txt
RUN apt-get update && \
    pip install --no-cache-dir --upgrade pip && \
    python3 -m pip wheel -w "${WHEEL_DIST}" -r /tmp/requirements.txt

### Финальная сборка образа
FROM python:3.11.2-slim
ARG WHEEL_DIST 
ARG WORK_DIR
COPY --from=build "${WHEEL_DIST}" "${WHEEL_DIST}"
WORKDIR "${WORK_DIR}"
## dnsutils - чтобы работал dig
RUN apt-get update && \
    apt-get install -y dnsutils && \
    pip install --no-cache-dir --upgrade pip && \
    pip --no-cache-dir install "${WHEEL_DIST}"/*.whl
ENV TZ=Europe/Moscow SETTINGS_PATH="/routes-to-vpn/"
COPY ./app "${WORK_DIR}"/app
RUN chmod +x "${WORK_DIR}"/app/main.py
COPY ./data "${WORK_DIR}"/data
CMD ["python", "app/main.py"]
