import json
from typing import List

from pydantic import BaseModel, HttpUrl
from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    """Класс Settings для получения данных из переменных окружения

    Args:
        BaseSettings (_type_): BaseSettings из pydantic_settings
    """
    vault_token: str
    vault_address: HttpUrl


settings = Settings() 


class Data(BaseModel):
    """Класс Data для получения данных из json-файла

    Args:
        BaseModel (_type_): _description_
    """
    domains: List[str]


def getData(jsonfile: str) -> Data:
    with open(jsonfile) as f:
        data = json.load(f)
    return Data(**data)
    # return Data.parse_obj(data)