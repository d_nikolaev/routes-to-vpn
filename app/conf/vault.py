import hvac
from urllib3 import disable_warnings, exceptions           # для отключения сообщений Warning о небезопасности сертификата

from conf.settings import settings

disable_warnings(exceptions.InsecureRequestWarning)        # Отключаем предупреждения Warning о небезопасности сертификата


#### path и mount_point. В случае необходимости внести корректировку #####
path = "/automation/mikrotik"    
mount_point='/secret-routers'
#### 

class Mikrotik:
    def __init__(self):
        env = settings
        self._address = env.vault_address
        self._token = env.vault_token
        self._data = self.__getDict()
        self.host = self.__getKey("ip")
        self.login = self.__getKey("login")
        self.password = self.__getKey("password")


    def __getDict(self) -> dict:
        """Получить словарь данных из Vault

        Returns:
            dict: словарь данных из Vault
        """
        client = hvac.Client(self._address, self._token, verify=False)
        response = client.secrets.kv.read_secret_version(mount_point=mount_point, path=path)


    def __getKey(self, key):
        return self._data.get(key, None)
    

mikrotik = Mikrotik()