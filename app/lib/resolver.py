#!./venv/bin/python
#-*- coding: utf-8 -*-
"""
Возращает список IP-адресов, которые ресолвятся для конкретного доменного имени
"""
from pprint import pprint
import ipaddress

import pydig


def getResourceIp(domain: str) -> set:
    """Получаем список IP адресов для конкретного домена с помощью dig

    Args:
        domain (str): наименование домена

    Returns:
        set: список IP адресов
    """
    result = []
    data = pydig.query(domain, 'A')
    for addr in data:
        try:
            addr = str(ipaddress.ip_address(addr))
            result.append(addr)
        except:
            continue
    return set(result)


if __name__=='__main__':
    domain = 'ast.tucny.com'
    print('Domain              :', domain)
    pprint(getResourceIp(domain))
