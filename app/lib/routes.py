import sys
import os
sys.path.append(os.getcwd())
import datetime
from typing import List

import routeros_api

from app.lib.resolver import getResourceIp
from app.conf.vault import mikrotik


#### IP-адрес VPN-ресурса и комментарий. В случае необходимости внести корректировку #####
gateway="10.0.0.150"
comment = "TO_VPN_"
#### 


class Routes():
    def __init__(self) -> None:
        self.routes = self.__getRoutes()


    def __getMessage(self) -> str:
        """Получить сообщение с датой и временем для дальнейшего вывода на экран (а-ля логи :))

        Returns:
            str: сообщение с датой и временем
        """
        return f"[{datetime.datetime.now().strftime('%d.%m.%Y %H:%M:%S')}]"


    def __getRoutes(self) -> List[routeros_api.RouterOsApiPool.get_api]:
        """Получить список маршрутов на Mikrotik

        Returns:
            List: список маршрутов
        """
        try:
            connection = routeros_api.RouterOsApiPool(mikrotik.host, username=mikrotik.login, password=mikrotik.password, plaintext_login=True)
            api = connection.get_api()
            routes = api.get_resource('/ip/route')
            return routes
        except:
            print(f"{self.__getMessage()} ERROR: Mikrotik - Authentication error")


    def __addRouteToVPN(self, ip: str, domain: str) -> None:
        """Добавить новый маршрут до VPN на Mikrotik

        Args:
            ip (str): ip-адрес ресурса 
            domain (str): доменное имя ресурса
        """
        ip += "/32"
        add_comment = f"{comment}{domain}"
        self.routes.add(dst_address = ip, gateway=gateway, comment = add_comment)


    def __removeRouteToVPN(self, id: str) -> None:
        """Удалить маршрут из списка маршрутов на Mikrotik

        Args:
            id (str): id маршрута
        """
        self.routes.remove(id=id)


    def __isDomainInRoutes(self, domain: str, route: dict) -> bool:
        if route.get("comment") is not None:
            if f"{comment}{domain}" == route["comment"]:
                return True
            
    
    def __getIdRoute(self, route) -> str:
        return route["id"]


    def addDomainToVPN(self, domain: str = "community.cisco.com"):
        """Основной метод. Обновление (добавление новых и удаление старых) маршрутов до ресурса

        Args:
            domain (str, optional): Доменное имя ресурса. Defaults to "community.cisco.com".
        """
        try:
            list_ip = getResourceIp(domain)
            routes_list = self.routes.get()     # вот тут преобразуем класс в список словарей для получения информации о маршрутах
        except:
            return
        
        if len(list_ip) < 1:
            print(f"{self.__getMessage()} ERROR: \"{domain}\" has not IP-adresses")
            return
        message = ''

        ### Удаляем старые маршруты
        try:
            for route in routes_list:
                if self.__isDomainInRoutes(domain, route):
                    self.__removeRouteToVPN(id=self.__getIdRoute(route))
            message += f"remove old and"
        except:
            print(f"{self.__getMessage()} ERROR: \"{domain}\" - remove old routes failed")

        ### Добавляем новые маршруты
        try:
            for ip in list_ip:
                self.__addRouteToVPN(ip, domain)
            print(f"{self.__getMessage()} \"{domain}\" - {message} add new routes")
        except:
            print(f"{self.__getMessage()} ERROR: \"{domain}\" - add new routes failed")
