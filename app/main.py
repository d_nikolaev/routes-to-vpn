from conf.settings import getData
from lib.routes import Routes


def main():
    routes = Routes()
    data = getData("data/domains.json")
    for domain in data.domains:
        routes.addDomainToVPN(domain=domain)


if __name__ == "__main__":
    main()